<?php

$debug = false;

session_start();
$size = [30, 30];

if (isset($_GET)) {
	if (isset($_GET['debug'])) {
		$debug = true;
	}
	if (isset($_GET['auto'])) {
		$sauto = htmlspecialchars($_GET['auto'], ENT_QUOTES);
	}
	if (isset($_GET['reset'])) {
		unset($_SESSION['cells']);
	}
		if (isset($_GET['generation'])) {
			$generation = htmlspecialchars($_GET['generation'], ENT_QUOTES);
		}
	}

function iTab($cells, $size, $randomize = true){
	for ($x=0; $x < $size[0]; $x++) { 
		$cells[$x] = array();
		for ($y=0; $y < $size[1]; $y++) { 
            if ($randomize && (bool) rand (0, 1)) {
                $cells[$x][$y] = true;
            }
            else {
                $cells[$x][$y] = false;
            }
		}
	}
	return $cells;
}

	function voisins($cells, $size, $cell){
		$x = $cell[0];
		$y = $cell[1];

		$next = 0;

		if ($x != 0 && $y != 0) {
			if ($cells[$x-1][$y-1]) {
				$next++;
			}
		}
		if ($x != 0) {
			if ($cells[$x-1][$y]) {
				$next++;
			}
		}
		if ($x != 0 && $y != $size[1]-1) {
			if ($cells[$x-1][$y-1]) {
				$next++;
			}
		}
		if ($y != 0) {
			if ($cells[$x][$y-1]) {
				$next	++;
			}
		}
		if ($y != $size[1]-1) {
			if ($cells[$x][$y+1]) {
				$next	++;
			}
		}
		if ($x != $size[0]-1 && $y != 0) {
			if ($cells[$x+1][$y-1]) {
				$next	++;
			}
		}
		if ($x != $size[0]-1) {
			if ($cells[$x+1][$y]) {
				$next	++;
			}
		}
		if ($x != $size[0]-1 && $y != $size[1]-1) {
			if ($cells[$x+1][$y+1]) {
				$next	++;
			}
		}
		return $next;
	}

	function nais($cells, $cell, $next){
		$x = $cell[0];
		$y = $cell[1];

		return($next === 3 && !$cells[$x][$y]);
	}

	function mortf($cells, $cell, $next){
		$x = $cell[0];
		$y = $cell[1];

		return(($next < 2 || $next > 3) && $cells[$x][$y]);
	}

	function changer($cells, $size){
		$pdt = ['birth'=> array(), 'death' => array()];

		for ($x=0; $x < $size[0]; $x++) { 
			for ($y=0; $y < $size[1]; $y++) { 
				$cell = [$x, $y];

				$next = voisins($cells, $size, $cell);

				if (nais($cells, $cell, $next)) {
					array_push($pdt['birth'], $cell);
				}
				if (mortf($cells, $cell, $next)) {
					array_push($pdt['death'], $cell);
				}
			}
		}
		return $pdt;
	}

function apply($cells, $pdt){
	foreach ($pdt['birth'] as $cell) {
		$x = $cell[0];
		$y = $cell[1];

		$cells[$x][$y] = true;
	}

	foreach ($pdt['death'] as $cell) {
			$x = $cell[0];
			$y = $cell[1];

			$cells[$x][$y] = false;
		}
	return $cells;
	}

function nwG ($cells, $size){
	$pdt = changer($cells, $size);
	$cells = apply($cells, $pdt);
	$_SESSION['current_generation']++;

	return $cells;
	}

if (!isset($_SESSION['cells'])) {
	$cells = iTab(array(), $size);

	$_SESSION['cells'] = $cells;
	$_SESSION['size'] = $size;
	$_SESSION['current_generation'] = 0;
}
else {
	if (isset($generation) && @$generation > $_SESSION['current_generation']) {
		$cells = $_SESSION['cells'];
		$size = $_SESSION['size'];

		for ($g = $_SESSION['current_generation']; $g < $generation ; $g++) { 
			$cells = nwG($cells, $size);
		}
	}
		else {
			$cells = nwG($_SESSION['cells'], $_SESSION['size']);
		}
			
	$_SESSION['cells'] = $cells;
}