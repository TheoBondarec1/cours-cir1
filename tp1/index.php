<?php require('game.php'); ?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8"></meta>
	<meta name='viewport' content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA Compatible" content="ie=edge">
	<?php if (isset($sauto)) { ?>
	<meta http-equiv="refresh" content="<?= $sauto ?>; url=./index.php?auto=<?= $sauto ?>" />
	<?php }?>
	<title>Jeu de La Vie</title>
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h1>Jeu De Laa Vie</h1>
	<?php include('template.html');?>
	<div id='actions'>
		<p>
			Generation :
			<?= $_SESSION['current_generation'];?>
				-
				<a href="./">Next Generation</a>
				-
				<a href="?reset">Reset</a>
		</p>
		Auto refresh:
		<?php if (isset($sauto)) { ?>
			<a href="./">Stop</a>
		<?php } else {?>
		<form method="get">
			<input type="number" name="auto" value="1" min="0.1" max="10" step="0.1">
			<input type="submit" name="Depart">
		</form>
		<?php } ?>
		<form method="get">Calcul Generation
			<input type="number" name="generation" value="<?=$_SESSION['current_generation'] + 1; ?>" min="<?= $_SESSION['current_generation']+1; ?>" max="999999">
			<input type="submit" name="Envoyer">
		</form>
	</div>
	<p>
		<?php if($debug){
			var_dump($_SESSION);
		}?>
	</p>
</body>

</html>